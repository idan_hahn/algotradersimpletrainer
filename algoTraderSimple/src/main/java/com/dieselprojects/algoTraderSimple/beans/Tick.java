package com.dieselprojects.algoTraderSimple.beans;

import java.time.ZonedDateTime;

public class Tick {

	private String ticker;
	private double open;
	private double high;
	private double low;
	private double close;
	private long volume;
	private ZonedDateTime time;
	private Object additionals;

	public Tick(String ticker, double open, double high, double low,
			double close, long volume, ZonedDateTime time) {
		super();
		this.ticker = ticker;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
		this.time = time;
	}

	public Tick(String ticker, double open, double high, double low,
			double close, long volume, ZonedDateTime time, Object additionals) {
		super();
		this.ticker = ticker;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
		this.time = time;
		this.additionals = additionals;
	}
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public long getVolume() {
		return volume;
	}

	public void setVolume(long volume) {
		this.volume = volume;
	}

	public ZonedDateTime getTime() {
		return time;
	}

	public void setTime(ZonedDateTime time) {
		this.time = time;
	}
	
	public Object getAdditionals() {
		return additionals;
	}

	public void setAdditionals(Object additionals) {
		this.additionals = additionals;
	}

	public String[] getFields(){
		String[] fields = new String[7];
		fields[0] = ticker;
		fields[1] = String.valueOf(open);
		fields[2] = String.valueOf(high);
		fields[3] = String.valueOf(low);
		fields[4] = String.valueOf(close);
		fields[5] = String.valueOf(volume);
		fields[6] = String.valueOf(time);
	
		return fields;
	}
}
