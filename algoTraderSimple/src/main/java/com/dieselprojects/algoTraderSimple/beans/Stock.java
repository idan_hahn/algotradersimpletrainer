package com.dieselprojects.algoTraderSimple.beans;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import com.dieselprojects.algoTraderSimple.support.TimeMan;
import com.dieselprojects.algoTraderSimple.support.Types.StockState;

public class Stock {

	private String ticker;
	private double price;
	private ZonedDateTime updateTime;
	private StockState state;

	private LinkedList<Tick> ticks;

	private HashMap<String,Object> additional;

	public Stock(String ticker) {
		super();
		this.ticker = ticker;
		
		price = 0.0;
		updateTime = TimeMan.getInstance().getCurrentTime();
		state = StockState.ACTIVE;
		ticks = new LinkedList<Tick>();
		
	}

	public void addTick(Tick tick){
		ticks.add(tick);
		updateTime = tick.getTime();
	}
	
	
	
	
	
	// -------------------------- //
	// -- Getters/Setters here -- //
	// -------------------------- //
	
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ZonedDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(ZonedDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public StockState getState() {
		return state;
	}

	public void setState(StockState state) {
		this.state = state;
	}

	public LinkedList<Tick> getTicks() {
		return ticks;
	}

	public void setTicks(LinkedList<Tick> ticks) {
		this.ticks = ticks;
	}

	public HashMap<String, Object> getAdditional() {
		return additional;
	}

	public void setAdditional(HashMap<String, Object> additional) {
		this.additional = additional;
	}
	
	
	
	
}
