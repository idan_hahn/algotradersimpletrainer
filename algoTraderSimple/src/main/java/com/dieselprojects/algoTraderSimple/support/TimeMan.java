package com.dieselprojects.algoTraderSimple.support;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeMan {

	private static TimeMan instance;
	
	public static TimeMan getInstance() {
		if (instance == null) {
			instance = new TimeMan();
		}
		return instance;
	}


	ZoneId id;
	ZonedDateTime time;
	
	private TimeMan() {
		
		id = ZoneId.of("America/New_York");
	
	}
	
	public ZonedDateTime getCurrentTime(){
		return ZonedDateTime.now(id);
	}
	
}
