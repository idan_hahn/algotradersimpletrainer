package com.dieselprojects.algoTraderSimple.support;

public class TrainerConfiguration {
	private static TrainerConfiguration instance;
	
	public static TrainerConfiguration getInstance() {
		if (instance == null) {
			instance = new TrainerConfiguration();
		}
		return instance;
	}
	
	private TrainerConfiguration() {}

	public String startDate;
	public String endDate;
	
	public String stocks[] = {"FSL"};
	

}
